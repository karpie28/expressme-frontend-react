/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux';
import constants from './constants';
// import actions from './authActions';

const initialState = ({
  login: false,
});

function authReducer(state = initialState, action) {
  switch (action.type) {
    case constants.LOG_IN:
      return ({
        login: action.data,
      });
    default:
      return state;
  }
}

/**
 * Creates the main reducer with the dynamically injected ones
 */
export default combineReducers({
  userAuth: authReducer,
});
