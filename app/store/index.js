import { compose, createStore } from 'redux';
import persistState from 'redux-localstorage';
import reducer from './reducer';

const initialState = {
  userAuth: false,
  accessToken: null,
};

const enhancer = compose(
  persistState(),
  ...(window.__REDUX_DEVTOOLS_EXTENSION__ ? [window.__REDUX_DEVTOOLS_EXTENSION__()] : [])
);

const store = createStore(
  reducer,
  initialState,
  enhancer,
);

export default store;
