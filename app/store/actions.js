import constants from './constants';

const UpdateLogState = (userAuth, accessToken) => ({
  type: constants.LOG_IN,
  userData: userAuth,
  accessData: accessToken,
});

export default {
  UpdateLogState,
};
