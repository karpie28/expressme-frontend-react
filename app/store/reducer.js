import constants from './constants';
import { initialState } from './index';

export default function (state = initialState, action) {
  switch (action.type) {
    case constants.LOG_IN:
      return ({
        userAuth: action.userData,
        accessToken: action.accessData,
      });
    default:
      return state;
  }
}
