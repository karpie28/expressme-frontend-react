/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React, { Component } from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import NotFoundPage from 'containers/NotFoundPage';
import { connect } from 'react-redux';

import homePage from '../../components/homePage';
import loginPage from '../../components/loginPage';
import registerPage from '../../components/registerPage';
import teamPage from '../../components/teamPage';
import contactPage from '../../components/contactPage';
import userPanel from '../../components/userPanel';
import activatePage from '../../components/activatePage';
import userPanelFriendOne from '../../components/userPanelFriendOne';
import resendActivationPage from '../../components/resendActivationPage';
import resetPassPage from '../../components/resetPassPage';
import sendResetPassPage from '../../components/sendResetPassPage';
import manageFriends from '../../components/userProfileManageFriends';
import allFriends from '../../components/allFriends';
import friendsInvites from '../../components/friendsInvites';
import searchFriends from '../../components/searchFriends';

const PrivateRoute = ({ component: Component, authed, ...rest }) => (
  <Route
    {...rest}
    render={(props) => (
    authed
      ? <Component {...props} />
        : <Redirect
          to="/login"
        />
    )}
  />
);

class App extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <div>
        <Switch>
          <Route exact path="/" component={homePage} />
          <Route path="/team" component={teamPage} />
          <Route path="/login" component={loginPage} />
          <Route path="/register" component={registerPage} />
          <Route path="/contact" component={contactPage} />
          <Route path="/resend" component={resendActivationPage} />
          <Route path="/activate/:activeToken" component={activatePage} />
          <Route path="/reset/" component={sendResetPassPage} />
          <Route path="/password/change/:changeToken" component={resetPassPage} />
          <PrivateRoute path="/user-panel/:userId" authed={this.props.userAuth} component={userPanelFriendOne} />
          <PrivateRoute path="/user-panel" authed={this.props.userAuth} component={userPanel} />
          <PrivateRoute path="/manage-friends" authed={this.props.userAuth} component={manageFriends} />
          <PrivateRoute path="/all-friends" authed={this.props.userAuth} component={allFriends} />
          <PrivateRoute path="/friend-invites" authed={this.props.userAuth} component={friendsInvites} />
          <PrivateRoute path="/search-friend" authed={this.props.userAuth} component={searchFriends} />
          <Route component={NotFoundPage} />
        </Switch>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    userAuth: state.userAuth,
    accessToken: state.accessToken,
  };
}

export default withRouter(connect(mapStateToProps)(App));


// export default App;
