/**
 *
 * UserPanel
 *
 */

import React from 'react';
// import style from './style.less';
import UserNav from '../../components/userPanelNav';
import UserAddFeed from '../../components/userPanelAddFeed';
import UserFeeds from '../../components/userPanelFeeds';
// import grid from '../../styles/grid.less';

class UserPanel extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <UserNav />
        <UserAddFeed />
        <UserFeeds />
      </div>
    );
  }
}

export default UserPanel;
