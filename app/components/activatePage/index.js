/**
 *
 * Activate Page
 *
 */
import React from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import style from './style.less';

class Register extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.state = {
      activationStatus: false,
      redirect: false,
    };
  }

  componentDidMount() {
    axios.get(`https://api.expressme.pl/api/auth/activate/${this.props.match.params.activeToken}`)
    .then((result) => {
      // console.log(result);
      this.setState({ activationStatus: true });
    })
    .catch((error) => {
      // console.log(error.message);
    });
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  }

  activationStatusLog = () => {
    if (this.state.activationStatus) {
      return (
        <div className={style['pnf-text']}>
          <div>Pomyślnie zaaktywowano konto.Możesz się teraz zalogować!</div>
          <button onClick={this.setRedirect} className={style['login-button']}>Zaloguj</button>
        </div>
      );
    } else {
      return (
        <div className={style['pnf-text']}>
          <div className={style['pnf-error']}>404</div>
          <div>Coś poszło nie tak!</div>
          <div> Twoj kod aktywacyjny wygasł lub został już aktywowany.</div>
        </div>
      );
    }
  }

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/login" />;
    }
  }

  render() {
    return (
      <div className={style.pnf}>
        { this.renderRedirect() }
        { this.activationStatusLog() }
      </div>
    );
  }
}

export default Register;
