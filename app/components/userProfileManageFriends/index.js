/**
 *
 * UserPanel Manage Friends
 *
 */

import React from 'react';
// import { Link } from 'react-router-dom';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import UserNav from '../../components/userPanelNav';
import style from './style.less';
import grid from '../../styles/grid.less';

class UserPanelManageFriends extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.state = {
      reverse: false,
    };
    this.loaded = false;
  }

  render() {
    return (
      <div>
        <UserNav />
        <div className={grid.container}>
          <Link to="all-friends" className={style['link-item']}>All friends</Link><br />
          <Link to="friend-invites" className={style['link-item']}>Your invitations</Link><br />
          <Link to="search-friend" className={style['link-item']}>Search friends</Link><br />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    accessToken: state.accessToken,
    userAuth: state.userAuth,
  };
}

// export default connect(mapStateToProps)(UserPanelManageFriends);
export default withRouter(connect(mapStateToProps)(UserPanelManageFriends));

// export default UserPanel;
