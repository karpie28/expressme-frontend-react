/**
 *
 * Contact
 *
 */

import React from 'react';
import GoogleMapReact from 'google-map-react';
import style from './style.less';
import grid from '../../styles/grid.less';
import Header from '../../components/homePageHeader';
import Footer from '../../components/homePageFooter';

const AnyReactComponent = ({ }) => <div className={style['map-box']}></div>;

class Contact extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <Header />
        <div className={[grid.container, style.box].join(' ')}>
          <main className={style.main}>
            <h2 className={style['header-title']}>Skontaktuj się z nami</h2>
            <div className={grid.row}>
              <div className={[grid['col-md-6'], style['footer-box']].join(' ')}>
                <div className={style['map-box']}>
                  <GoogleMapReact
                    defaultCenter={this.props.center}
                    defaultZoom={this.props.zoom}
                    style={{ height: '300px' }}
                  >
                    <AnyReactComponent
                      lat={59.955413}
                      lng={30.337844}
                      text={'Google Map'}
                    />
                  </GoogleMapReact>
                </div>
              </div>
              <div className={[grid['col-md-6'], style['footer-box']].join(' ')}>
                <h3 className={style['footer-box-heading']}>Wydział Matematyki i Informatyki UAM</h3>
                <p className={style['footer-logo-alt']}>
                  <a href="mailto:info@expressme.com">info@expressme.com</a>
                </p>
              </div>
            </div>
          </main>
        </div>
        <Footer />
      </div>
    );
  }
}
Contact.defaultProps = {
  center: { lat: 52.4666952, lng: 16.9219273 },
  zoom: 16,
};
export default Contact;
