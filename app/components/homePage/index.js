/**
 *
 * Home
 *
 */

import React from 'react';
// import style from './style.less';
import Header from '../../components/homePageHeader';
import Main from '../../components/homePageMain';
import Footer from '../../components/homePageFooter';

class homePage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  initToken() {
    if (!window.localStorage.getItem('auth_token')) {
      window.localStorage.setItem('auth_token', false);
    }
    if (!window.localStorage.getItem('access_token')) {
      window.localStorage.setItem('access_token', '');
    }
  }

  render() {
    return (
      <div>
        <Header />
        <Main />
        <Footer />
        {this.initToken()}
      </div>
    );
  }
}


export default homePage;
