/**
 *
 * Activate Page
 *
 */
import React from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import style from './style.less';

function validate(password, rpassword) {
  const errors = [];
  if (password !== rpassword) {
    errors.push('Podane hasła nie są identyczne!');
  } else if (password < 8) {
    errors.push('Podane hasło jest za krótkie, minimum 8 znaków!');
  }
  return errors;
}

class Register extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.state = {
      activationStatus: null,
      redirect: false,
      password: '',
      rpassword: '',
      errors: [],
    };
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit = (e) => {
    e.preventDefault();
    const { password, rpassword } = this.state;
    const errors = validate(password, rpassword);
    if (errors.length > 0) {
      this.setState({ errors });
    } else {
      axios.post(`https://api.expressme.pl/api/auth/password/change/${this.props.match.params.changeToken}`, { password })
        .then((result) => {
          console.log(result);
          this.setState({
            activationStatus: true,
          });
        })
        .catch((error) => {
          errors.push('Podano błędne dane lub twój kod został już wykorzystany.');
          console.log(error.message);
        });
    }
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  }

  activationStatusLog = () => {
    if (this.state.activationStatus) {
      return (
        <div className={style['resend-modal']}>
          <div>Pomyślnie zresetowano hasło. Możesz sie teraz zalogować</div>
          <button onClick={this.setRedirect} className={style['login-button']}>Zaloguj</button>
        </div>
      );
    }
  }

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/login" />;
    }
  }

  render() {
    const { errors } = this.state;
    return (
      <div className={style.resend}>
        {this.activationStatusLog()}
        <div className={style['form-wrapper']}>
          <h1 className={style.title}>ExpressMe</h1>
          <p className={style.alt}>Reset your passowrd</p>
          <form method="post" onSubmit={this.onSubmit} className={style['form-box']}>
            { errors.map((error) => (
              <div key={error} className={style['error-box']}>
                <p>Błąd: {error}</p>
              </div>
            ))}
            <input
              value={this.state.password}
              onChange={(evt) => this.setState({ password: evt.target.value })}
              className={style.input}
              type="password"
              name="password"
              placeholder="Hasło"
            />
            <input
              value={this.state.rpassword}
              onChange={(evt) => this.setState({ rpassword: evt.target.value })}
              className={style.input}
              type="password"
              name="rpassword"
              placeholder="Powtórz hasło"
            />
            <button type="submit" className={style['submit-button']}>Send</button>
          </form>
        </div>
      </div>
    );
  }
}

export default Register;
