/**
 *
 * UserPanel all Friends
 *
 */

import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import style from './style.less';
import grid from '../../styles/grid.less';
import UserNav from '../../components/userPanelNav';

class UserPanelManageFriends extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.state = {
      reverse: false,
      friends: [],
    };
    this.loaded = false;
    // this.showFriends = this.showFriends.bind(this);
  }

  getFriends() {
    const Authorization = `Bearer ${this.props.accessToken}`;
    axios.get('https://api.expressme.pl/api/friends', {
      headers: { Authorization: Authorization },
    })
      .then((result) => {
        this.loaded = true;
        // console.log(result);
        this.setState({ friends: result.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  deleteFriend(friendId) {
    const Authorization = `Bearer ${this.props.accessToken}`;
    axios.delete(`https://api.expressme.pl/api/friends/${friendId}`, {
      headers: { Authorization: Authorization },
    })
      .then((result) => {
        console.log(result);
        // this.setState({ friends: result.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    !this.loaded && this.getFriends();
    return (
      <div>
        <UserNav />
        <div className={grid.container}>
          <div className={style.all}>
            {
              this.state.friends && this.state.friends.map((friend, i) => (
                <div key={i} className={style.card}>
                  <div className={style.name}>
                    {console.log(friend)}
                    {friend.name}
                  </div>
                  <div className={style.surname}>
                    {friend.surname}
                  </div>
                  <div className={style.email}>
                    {friend.email}
                  </div>
                  <button
                    className={style.delete}
                    onClick={() => this.deleteFriend(friend.id)}
                  >
                    X
                  </button>
                </div>
              )
            )}
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    accessToken: state.accessToken,
    userAuth: state.userAuth,
  };
}

export default connect(mapStateToProps)(UserPanelManageFriends);

// export default UserPanel;
