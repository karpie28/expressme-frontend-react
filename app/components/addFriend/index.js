/**
 *
 * AddFriend
 *
 */

import React from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import style from './style.less';
import grid from '../../styles/grid.less';
import Header from '../../components/userPanelNav';

function validate(name, surname) {
  const errors = [];

  if (name === '' || surname === '') {
    errors.push('Wypełnij wszystkie pola!');
  } else {
    errors.push('Ups coś poszło nie tak. Spróbuj ponownie.');
  }
  return errors;
}

class addFriend extends React.Component {
  constructor() {
    super();
    this.state = {
      name: '',
      surname: '',
      errors: [],
      redirect: false,
    };
  }

  onSubmit = (e) => {
    e.preventDefault();
    const ApiToken = window.localStorage.getItem('access_token');
    const { name, surname } = this.state;
    axios({
      method: 'post',
      url: 'https://api.expressme.pl/api/friends',
      data: { name, surname },
      headers: {
        Authorization: 'Bearer ' + ApiToken,
      },
    }).then((result) => {
      console.log('Dodano');
      this.setRedirect();
    })
      .catch((error) => {
        const errors = validate(name, surname);
        if (errors.length > 0) {
          this.setState({ errors });
        }
      });
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  }

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/user-panel" />;
    }
  }

  render() {
    const { errors } = this.state;
    return (
      <div>
        { this.renderRedirect() }
        <Header />
        <div className={grid.container}>
          <form
            method="POST"
            onSubmit={this.onSubmit}
            className={style['add-form']}
          >
            { errors.map((error) => (
              <div className={style['error-box']}>
                <p key={error}>Błąd: {error}</p>
              </div>
            ))}
            <h2 className={style['add-title']}>Dodaje znajomego</h2>
            <input
              value={this.state.name}
              onChange={(evt) => this.setState({ name: evt.target.value })}
              className={style['add-input']}
              type="text"
              name="name"
              placeholder="Imię"
            />
            <input
              value={this.state.surname}
              onChange={(evt) => this.setState({ surname: evt.target.value })}
              className={style['add-input']}
              type="text"
              name="surname"
              placeholder="Nazwisko"
            />
            <button className={style['add-button']}>Dodaj</button>
          </form>
          <div className={style['res-message']}>
            {this.state.res}
          </div>
        </div>
      </div>
    );
  }
}

export default addFriend;
