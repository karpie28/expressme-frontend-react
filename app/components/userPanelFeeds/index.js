/**
 *
 * UserPanel Feeds
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import style from './style.less';
import grid from '../../styles/grid.less';

class UserPanelFeeds extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.state = {
      reverse: false,
    };
    this.loaded = false;
  }

  getFeeds() {
    const Authorization = `Bearer ${this.props.accessToken}`;
    axios.get('https://api.expressme.pl/api/feeds', {
      headers: { Authorization: Authorization },
    })
      .then((result) => {
        this.loaded = true;
        console.log(result);
        this.setState({ feeds: result.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    !this.loaded && this.getFeeds();
    return (
      <div className={grid.container}>
        <div className={style['boxes-wrapper']}>
          <div className={grid.row}>
            {
              this.state.feeds && this.state.feeds.map((feed) => (
                <div key={feed.feed_id} className={grid['col-md-6']}>
                  {/* <Link to={{ pathname: '/user-panel/' + feed.feed_id }}> */}
                  <div className={style.card}>
                    <div className={style['user-name']}> {feed.full_name} </div>
                    <img src={feed.image} className={style['user-img']} alt="image-name" />
                    <div className={style['emotions-wrapper']}>
                      Emocje ze zdjęcia: {feed.imageEmotion} <br />
                      Emocje z opisu: {feed.textEmotion}
                    </div>
                  </div>
                  {/* </Link> */}
                </div>
              )
            )}
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    accessToken: state.accessToken,
    userAuth: state.userAuth,
  };
}

export default connect(mapStateToProps)(UserPanelFeeds);

