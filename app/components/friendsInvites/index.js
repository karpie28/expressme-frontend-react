/**
 *
 * UserPanel Manage Friends
 *
 */

import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import style from './style.less';
import grid from '../../styles/grid.less';
import UserNav from '../../components/userPanelNav';

class UserPanelManageFriends extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.state = {
      reverse: false,
      invits: [],
    };
    this.loaded = false;
  }

  getInvits() {
    const Authorization = `Bearer ${this.props.accessToken}`;
    axios.get('https://api.expressme.pl/api/friends/invitation', {
      headers: { Authorization: Authorization },
    })
      .then((result) => {
        this.loaded = true;
        this.setState({ invits: result.data });
        console.log(this.state.invits);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    !this.loaded && this.getInvits();
    return (
      <div>
        <UserNav />
        <div className={grid.container}>
          <div className={style.wrapper}>
            {
              this.state.invits && this.state.invits.map((item, i) => (
                <div key={i} className={style.card}>
                  <div className={style.name}>
                    Invite from: {item.user_id_inviting}
                  </div>
                  <div className={style['button-wrapper']}>
                    <button className={style.add}>
                      Add
                    </button>
                    <button className={style.reject}>
                      Reject
                    </button>
                    <button className={style.block}>
                      Block
                    </button>
                  </div>
                </div>
              )
            )}
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    accessToken: state.accessToken,
    userAuth: state.userAuth,
  };
}

export default connect(mapStateToProps)(UserPanelManageFriends);

// export default UserPanel;
