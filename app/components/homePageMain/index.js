/**
 *
 * MainFrame
 *
 */

import React from 'react';
import style from './style.less';
import grid from '../../styles/grid.less';
import relations from '../../images/1.png';
import mood from '../../images/2.png';
import meetings from '../../images/3.png';

class MainFrame extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div className={style.background}>
        <div className={grid.container}>
          <main className={style.main}>
            <h2 className={style['header-title']}>O aplikacji</h2>
            <p className={style['header-alt']}>
              Stworzyliśmy aplikację mówiącą o ostatnim humorze znajomych.
              <br />Oto cele które nam przyświecały w tworzeniu aplikacji.
            </p>
            <div className={grid.row}>
              <div className={[grid['col-md-4'], style['footer-box']].join(' ')}>
                <img className={style['main-img']} src={relations} alt="Relations" />
                <h3 className={style['footer-box-heading']}>Relacje międzyludzkie</h3>
                <p className={style['footer-logo-alt']}>
                  Każdy człowiek ma potrzebę rozmowy, aplikacja powinna ją ułatwiać podając z kim dziś powinniśmy porozmawiać i o czym.
                </p>
              </div>
              <div className={[grid['col-md-4'], style['footer-box']].join(' ')}>
                <img className={style['main-img']} src={mood} alt="Mood" />
                <h3 className={style['footer-box-heading']}>Nastrój</h3>
                <p className={style['footer-logo-alt']}>
                  Użytkownik powinien otrzymać informację w jakim nastroju jest jego rozmówca. Użytkownik powinien też otrzymać propozycję tematów do rozmowy w zależności od humoru rozmówcy.
                </p>
              </div>
              <div className={[grid['col-md-4'], style['footer-box']].join(' ')}>
                <img className={style['main-img']} src={meetings} alt="Meetings" />
                <h3 className={style['footer-box-heading']}>Spotkania</h3>
                <p className={style['footer-logo-alt']}>
                  Otoczenie ma wpływ na nastrój w jakim obecnie się znajdujemy. Aplikacja proponuje miejsca spotkań w zależności od humoru.
                </p>
              </div>
            </div>
          </main>
        </div>
      </div>
    );
  }
}

export default MainFrame;
