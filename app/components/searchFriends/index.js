/**
 *
 * UserPanel Manage Friends
 *
 */

import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import style from './style.less';
import grid from '../../styles/grid.less';
import UserNav from '../../components/userPanelNav';

class UserPanelManageFriends extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.state = {
      reverse: false,
      searchName: null,
    };
    this.loaded = false;
  }

  getFriends() {
    const searchName = this.state.searchName;
    const Authorization = `Bearer ${this.props.accessToken}`;
    axios.get(`https://api.expressme.pl/api/friends/search?search=${searchName}`, {
      headers: { Authorization: Authorization },
    })
      .then((result) => {
        this.loaded = true;
        this.setState({ friends: result.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  addNewFriend(friendId) {
    const Authorization = `Bearer ${this.props.accessToken}`;
    axios.post('https://api.expressme.pl/api/friends/invitation', {
      headers: { Authorization: Authorization },
      body: { friendId },
    })
    .then((result) => {
      console.log(result);
      // this.setState({ friends: result.data });
    })
    .catch((error) => {
      console.log(error);
    });
  }

  render() {
    return (
      <div>
        <UserNav />
        <div className={grid.container}>
          <h2>Search friends</h2>
          <input className={style.input} onChange={(evt) => this.setState({ searchName: evt.target.value })} type="text" />
          <button className={style.button} onClick={() => this.getFriends()}>Szukaj</button>
          <div className={style['search-list']}>
            {
                this.state.friends && this.state.friends.map((friend, i) => (
                  <div key={i} className={style.card}>
                    <div className={style.name}>
                      {console.log(friend)}
                      {friend.name}
                    </div>
                    <div className={style.surname}>
                      {friend.surname}
                    </div>
                    <div className={style.email}>
                      {friend.email}
                    </div>
                    <button
                      onClick={() => this.addNewFriend(friend.id)}
                      className={style['add-friend']}
                    >Add Friend</button>
                  </div>
                )
              )}
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    accessToken: state.accessToken,
    userAuth: state.userAuth,
  };
}

export default withRouter(connect(mapStateToProps)(UserPanelManageFriends));

// export default UserPanel;
