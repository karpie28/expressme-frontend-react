/**
 *
 * UserPanel
 *
 */

import React from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import style from './style.less';
import grid from '../../styles/grid.less';
import userIco from '../../images/user.svg';

class UserPanel extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.state = {
      reverse: false,
    };
    this.loaded = false;
  }

  getFriends() {
    const ApiToken = window.localStorage.getItem('access_token');
    const Authorization = `Bearer ${ApiToken}`;
    axios.get('https://api.expressme.pl/api/friends', {
      headers: { Authorization: Authorization },
    })
      .then((result) => {
        this.loaded = true;
        // console.log(result);
        this.setState({ friends: result.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    !this.loaded && this.getFriends();
    return (
      <div className={grid.container}>
        <div className={style['boxes-wrapper']}>
          <div className={grid.row}>
            {
              this.state.friends && this.state.friends.map((friend) => (
                <div className={grid['col-md-6']}>
                  <Link to={{ pathname: '/user-panel/' + friend.id }}>
                    <div className={style.card}>
                      <div className={style['card-user']}>
                        {friend.name} {friend.surname}
                      </div>
                      <div className={style['friend-emoji']}>
                        {friend.name} wydaje się być ostatnio: <span className={style.emotion}>{this.getEmotion(friend)}! </span>
                      </div>
                      <div className={style['photo-title']}>
                        Zdjęcia
                      </div>
                      {
                        friend.last_photos.map((photo) => (
                          <div className={style.single}>
                            <img src={photo.uploaded_photo} className={style['single-photo']} alt={'img'} />
                            <div className={style['emotions-wrapper']}>
                              Na tym zdjęciu {friend.name} jest:
                              <br /><br />
                              {
                                (JSON.parse(photo.emotions)).map(emotion => (
                                  <div className={style['single-emotion']}>
                                    <span className={style['emo-color']}>{emotion.Type}</span> w {emotion.Confidence.toFixed(1)}%
                                  </div>
                                )
                              )}
                            </div>
                          </div>
                        )
                      )}
                    </div>
                  </Link>
                </div>
              )
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default UserPanel;
