import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Link, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import style from './style.less';
import actions from './../../store/actions';

function validate(email, password) {
  const errors = [];

  if (email.length === 0 || password.length === 0) {
    errors.push('Uzupełnij pola!');
  } else if ((email.split('').filter((x) => x === '@') || (email.indexOf('.') === -1)).length !== 1) {
    errors.push('Niepoprawny format email!');
  }
  return errors;
}

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      errors: [],
      redirect: false,
      accessToken: '',
      userAuth: false,
    };
    this.onSubmit = this.onSubmit.bind(this);
    // this.UpdateLogState = this.props.UpdateLogState.bind(this);
    // this.UpdateLogState = this.UpldateLogState.bind(this)
  }

  onSubmit = (e) => {
    e.preventDefault();
    const { email, password } = this.state;
    const errors = validate(email, password);
    if (errors.length > 0) {
      this.setState({ errors });
    } else {
      axios.post('https://api.expressme.pl/api/auth/login', { email, password })
        .then((result) => {
          this.setState({
            redirect: true,
            accessToken: result.data.access_token,
            userAuth: true,
          });
          this.props.UpdateLogState(this.state.userAuth, this.state.accessToken);
        })
        .catch((error) => {
          console.log(error);
          errors.push('Podano błędne dane!');
        });
    }
  }

  alreadyLogged = () => {
    if (this.props.userAuth) {
      return <Redirect to="/user-panel" />;
    }
  }

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/user-panel" />;
    }
  }

  render() {
    const { errors } = this.state;
    return (
      <div className={style.login}>
        { this.alreadyLogged() }
        { this.renderRedirect() }
        <div className={style['form-wrapper']}>
          <h1 className={style.title}>ExpressMe</h1>
          <p className={style.alt}>Login to enjoy emotions with us</p>
          <form method="post" onSubmit={this.onSubmit} className={style['form-box']}>
            { errors.map((error) => (
              <div key={error} className={style['error-box']}>
                <p>Błąd: {error}</p>
              </div>
            ))}
            <input
              value={this.state.email}
              onChange={(evt) => this.setState({ email: evt.target.value })}
              className={style.input}
              type="text"
              name="email"
              placeholder="E-mail"
            />
            <input
              value={this.state.password}
              onChange={(evt) => this.setState({ password: evt.target.value })}
              className={style.input}
              type="password"
              name="password"
              placeholder="Password"
            />
            <button type="submit" className={style['submit-button']}>Log In</button>
          </form>
          <div className={style.links}>
            <div className={style.account}>
              You don&#39;t have account? <Link to="/register" className={style['account-link']}>Register</Link> now!
            </div>
            <Link to="/" className={style['go-back']}>Go back to home page</Link>
          </div>
          <div className={style['buttons-spacing']}>
            <Link to="/resend" className={style['resend-button']}>Resend activation link.</Link>
            <Link to="/reset" className={style['password-button']}>Forgot password?</Link>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    accessToken: state.accessToken,
    userAuth: state.userAuth,
  };
}

const mapDispatchToProps = (dispatch) => ({
  UpdateLogState: (userAuth, accessToken) => {
    dispatch(actions.UpdateLogState(userAuth, accessToken));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
