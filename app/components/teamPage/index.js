/**
 *
 * Team
 *
 */

import React from 'react';
import style from './style.less';
import grid from '../../styles/grid.less';
import kp from '../../images/kp.jpg';
import ks from '../../images/ks.jpg';
import js from '../../images/js.jpg';
import gs from '../../images/gs.jpg';
import Header from '../../components/homePageHeader';
import Footer from '../../components/homePageFooter';


class Team extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <Header />
        <div className={grid.container}>
          <main className={style.main}>
            <h2 className={style['header-title']}>O Teamie</h2>
            <p className={style['header-alt']}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu lorem a justo semper pharetra id ut leo.
              Quisque rutrum finibus quam eu cursus. Nunc consectetur et elit a bibendum.
            </p>
            <div className={grid.row}>
              <div className={[grid['col-md-3'], style['footer-box']].join(' ')}>
                <img className={style['main-img']} src={kp} alt="Karol Piekarski" />
                <h3 className={style['footer-box-heading']}>Karol Piekarski</h3>
                <p className={style['footer-logo-alt']}>
                  Team leader. Backend Developer.
                </p>
              </div>
              <div className={[grid['col-md-3'], style['footer-box']].join(' ')}>
                <img className={style['main-img']} src={ks} alt="Kasper Spychała" />
                <h3 className={style['footer-box-heading']}>Kasper Spychała</h3>
                <p className={style['footer-logo-alt']}>
                  Mobile Developer.
                </p>
              </div>
              <div className={[grid['col-md-3'], style['footer-box']].join(' ')}>
                <img className={style['main-img']} src={js} alt="Jakub Siedlecki" />
                <h3 className={style['footer-box-heading']}>Jakub Siedlecki</h3>
                <p className={style['footer-logo-alt']}>
                  Frontend Developer.
                </p>
              </div>
              <div className={[grid['col-md-3'], style['footer-box']].join(' ')}>
                <img className={style['main-img']} src={gs} alt="Gerard Smętek" />
                <h3 className={style['footer-box-heading']}>Gerard Smętek</h3>
                <p className={style['footer-logo-alt']}>
                  UX/UI i Frontend Developer.
                </p>
              </div>
            </div>
          </main>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Team;
