/**
 *
 * UserPanel Feeds
 *
 */

import React from 'react';
// import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import style from './style.less';
// import grid from '../../styles/grid.less';

class UserPanelAddFeed extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.state = {
      description: null,
      selectedFile: null,
    };
    this.postFeed = this.postFeed.bind(this);
  }

  postFeed = () => {
    const Authorization = `Bearer ${this.props.accessToken}`;
    console.log(this.state.selectedFile);
    const fd = new FormData();
    fd.append('image', this.state.selectedFile, this.state.selectedFile.name);
    axios.post('https://api.expressme.pl/api/feeds', { fd }, {
      headers: { Authorization: Authorization },
    })
      .then((result) => {
        console.log(result);
      })
      .catch((error) => {
        console.log(error);
      });
  }

//   let data = new FormData();
// data.append('file', file, file.fileName);

// return (dispatch) => {
// axios.post(URL, data, {
//   headers: {
//     'accept': 'application/json',
//     'Accept-Language': 'en-US,en;q=0.8',
//     'Content-Type': multipart/form-data; boundary=${data._boundary},
//   }
// })
//   .then((response) => {
//     //handle success
//   }).catch((error) => {
//     //handle error
//   });
// };}



  render() {
    return (
      <div className={style['add-feed']}>
        <textarea type="text" onChange={(evt) => this.setState({ description: evt.target.value })} />
        <input type="file" onChange={(evt) => this.setState({ selectedFile: evt.target.files[0] })} />
        <button onClick={() => this.postFeed()}>Upload</button>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    accessToken: state.accessToken,
    userAuth: state.userAuth,
  };
}

export default connect(mapStateToProps)(UserPanelAddFeed);

