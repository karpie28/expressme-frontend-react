// import React from 'react';
// import { Route, Redirect } from 'react-router-dom';

// // const PrivateRoute = ({ component: Component, authed, ...rest }) => (
// //   <Route
// //     {...rest}
// //     render={(props) => (
// //       authed
// //         ? <Component {...props} />
// //         : <Redirect to="/login" />
// //     )}
// //   />
// // );

// // old private route
// // const authToken = window.localStorage.getItem('auth_token');
// const PrivateRoute = ({ component: Component, authed, ...rest }) => (
//   <Route
//     {...rest}
//     render={(props) => (
//     this.props.authToken
//       ? <Component {...props} />
//         : <Redirect
//           to="/login"
//         />
//     )}
//   />
// );

// export default PrivateRoute;
