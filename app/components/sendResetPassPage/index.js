/**
 *
 * Password reset Page
 *
 */
import React from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import style from './style.less';

function validate(email) {
  const errors = [];
  if (email.length === 0) {
    errors.push('Uzupełnij pola!');
  } else if ((email.split('').filter((x) => x === '@') || (email.indexOf('.') === -1)).length !== 1) {
    errors.push('Niepoprawny format email!');
  }
  return errors;
}

class Password extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.state = {
      email: '',
      errors: [],
      resend: false,
    };
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit = (e) => {
    e.preventDefault();
    const { email } = this.state;
    const errors = validate(email);
    if (errors.length > 0) {
      this.setState({ errors });
    } else {
      axios.post('https://api.expressme.pl/api/auth/password/reset', { email })
        .then((result) => {
          console.log(result);
          this.setState({
            resend: true,
          });
        })
        .catch((error) => {
          console.log(error);
          errors.push('Podano błędne dane!');
        });
    }
  }

  renderResendModal = () => {
    if (this.state.resend) {
      return (
        <div className={style['resend-modal']}>
          Link do zresetowania twojego hasła został wysłany na maila.
          <Link to="/login" className={style['go-back']}>Go to login page.</Link>
        </div>
      );
    }
  }

  render() {
    const { errors } = this.state;
    return (
      <div className={style.resend}>
        { this.renderResendModal() }
        <div className={style['form-wrapper']}>
          <h1 className={style.title}>ExpressMe</h1>
          <p className={style.alt}>Reset your passowrd</p>
          <form method="post" onSubmit={this.onSubmit} className={style['form-box']}>
            { errors.map((error) => (
              <div key={error} className={style['error-box']}>
                <p>Błąd: {error}</p>
              </div>
            ))}
            <input
              value={this.state.email}
              onChange={(evt) => this.setState({ email: evt.target.value })}
              className={style.input}
              type="text"
              name="email"
              placeholder="E-mail"
            />
            <button type="submit" className={style['submit-button']}>Reset</button>
          </form>
        </div>
      </div>
    );
  }
}

export default Password;
