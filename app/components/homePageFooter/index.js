/**
 *
 * Home Page Footer
 *
 */

import React from 'react';
import style from './style.less';
import grid from '../../styles/grid.less';

class Footer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <footer className={style.footer}>
        <div className={grid.container}>
          <div className={grid.row}>
            <div className={grid['col-md-4']}>
              <a href="" className={style['footer-logo']}>ExpressMe</a>
              <p className={style['footer-logo-alt']}>
                Z nami twój humor jest bezpieczny.
              </p>
            </div>
            <div className={[grid['col-md-4'], style['footer-box']].join(' ')}>
              <h3 className={style['footer-box-heading']}>ExpressMe</h3>
              <ul>
                <li><a href="" className={style['footer-box-link']}>Strona główna</a></li>
                <li><a href="" className={style['footer-box-link']}>O aplikacji</a></li>
                <li><a href="" className={style['footer-box-link']}>Opinie</a></li>
              </ul>
            </div>
            <div className={[grid['col-md-4'], style['footer-box']].join(' ')}>
              <h3 className={style['footer-box-heading']}>Kontakt</h3>
              <ul>
                <li><a href="mailto:info@expressme.com" className={[style['footer-box-link'], style['contact-link']].join(' ')}>info@expressme.com</a></li>
              </ul>
            </div>
          </div>
          <div className={style['socials-wrapper']}>
            <ul className={style.socials}>
              <li className={style['socials-item']}><a href="" className={[style['socials-item-link'], style.fb].join(' ')}><i className={['fab', 'fa-facebook-f'].join(' ')}></i></a></li>
              <li className={style['socials-item']}><a href="" className={[style['socials-item-link'], style.li].join(' ')}><i className={['fab', 'fa-linkedin-in'].join(' ')}></i></a></li>
              <li className={style['socials-item']}><a href="" className={[style['socials-item-link'], style.tw].join(' ')}><i className={['fab', 'fa-twitter'].join(' ')}></i></a></li>
            </ul>
          </div>
        </div>
      </footer>
    );
  }
}


export default Footer;
