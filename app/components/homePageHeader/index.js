/**
 *
 * Home Page Header
 *
 */

import React from 'react';
import style from './style.less';
import grid from '../../styles/grid.less';
import Nav from '../homePageNav';
import headerImg from '../../images/header-img.jpg';


class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <header className={style.header}>
        <div className={grid.container}>
          <Nav />
          <h1 className={style['header-title']}>
            ExpressMe wnosi relacje na zupełnie nowy EMOCJONALNY poziom.
          </h1>
          <p className={style['header-alt']}>
            Stworzyliśmy aplikacje podpowiadającą nastrój Twego rozmówcy.
          </p>
          <img src={headerImg} className={style['header-img']} alt="Header" />
        </div>
      </header>
    );
  }
}

export default Header;
