/**
 *
 * Home Page Nav
 *
 */

import React from 'react';
import { Link } from 'react-router-dom';
import style from './style.less';

const navItems = { 'Nasza Aplikacja': '/', 'O nas': 'team', Kontakt: 'contact', Zaloguj: 'login', Rejestracja: 'register' };

class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <nav className={style.nav}>
        <a href="" className={style.logo}>ExpressMe</a>
        <ul className={style.navbar}>
          {
            Object.keys(navItems).map((item) => (
              <li key={item}>
                <Link to={navItems[item]} className={style['navbar-link']}>{item}</Link>
              </li>)
            )
          }
        </ul>
      </nav>
    );
  }
}


export default Header;
