/**
 *
 * UserPanel Navbar
 *
 */

import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import style from './style.less';
import grid from '../../styles/grid.less';
import actions from './../../store/actions';

class UserPanelNav extends React.Component { // eslint-disable-line react/prefer-stateless-function

  logOut = () => {
    this.props.UpdateLogState(false, null);
  }

  render() {
    return (
      <div className={style['nav-wrapper']}>
        <div className={[grid.container, style.container].join(' ')}>
          <nav className={style.navbar}>
            <Link to="/user-panel">
              <div className={style.brand}>ExpressMe</div>
            </Link>
            <ul className={style['navbar-list']}>
              <li><Link to="/user-panel" className={style['nav-link']}>Start</Link></li>
              <li><Link to="/manage-friends" className={style['nav-link']}>Zarządzaj znajomymi</Link></li>
              <li><Link to="/" className={style['nav-link']} onClick={this.logOut}>Wyloguj</Link></li>
            </ul>
          </nav>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    accessToken: state.accessToken,
    userAuth: state.userAuth,
  };
}

const mapDispatchToProps = (dispatch) => ({
  UpdateLogState: (userAuth, accessToken) => {
    dispatch(actions.UpdateLogState(userAuth, accessToken));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(UserPanelNav);

// export default UserPanelNav;
