/**
 *
 * UserPanelFriendOne
 *
 */

import React from 'react';
import axios from 'axios';
import FileBase64 from 'react-file-base64';
import UserNav from '../../components/userPanelNav';
import style from './style.less';
import grid from '../../styles/grid.less';
import userIco from '../../images/user.svg';

function validate() {
  const errors = [];
  errors.push('Przesłane zdjęcie nie spełnia kryteriów');
  return errors;
}


class userPanelFriendOne extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.state = {
      files: [],
      errors: [],
    };
    this.loaded = false;
  }

  getFiles(files) {
    console.log(files);
    this.setState({ files: files });
    this.postPhoto();
  }

  getFriend() {
    const ApiToken = window.localStorage.getItem('access_token');
    const Authorization = `Bearer ${ApiToken}`;
    axios.get(`https://api.expressme.pl/api/friends/${this.props.match.params.userId}`, {
      headers: { Authorization: Authorization },
    })
      .then((result) => {
        this.loaded = true;
        console.log(result);
        this.setState({ friend: result.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  postPhoto() {
    const ApiToken = window.localStorage.getItem('access_token');
    const Authorization = `Bearer ${ApiToken}`;
    axios.post('https://api.expressme.pl/api/friends/' + this.props.match.params.userId + '/photo', { photo: this.state.files.base64 }, {
      headers: { Authorization: Authorization },
    })
      .then((result) => {
        console.log(result);
        window.location.reload();
      })
      .catch((error) => {
        console.log(error);
        const errors = validate();
        if (errors.length > 0) {
          this.setState({ errors });
        }
      });
  }

  render() {
    !this.loaded && this.getFriend();
    const friend = this.state.friend;
    const errors = this.state.errors;
    return (
      <div>
        <UserNav />
        <div className={grid.container}>
          { errors.map((error) => (
            <div className={style['error-box']}>
              <p key={error}>Błąd: {error}</p>
            </div>
          ))}
          <div className={style['upload-photo']}>
            Kliknij aby dodać zdjęcie
            <label>
              <FileBase64
                multiple={false}
                onDone={this.getFiles.bind(this)}
              />
            </label>
          </div>
          {friend &&
          <div>
            <div>
              <div className={style.user}>
                
                {friend.name} {friend.surname}
              </div>
              <div className={style['friend-emoji']}>
                {friend.name} wydaje się być ostatnio: <span className={style.emotion}>Wesoły!</span> Napisz do niego, to dobry czas na rozmowę.
                      </div>
              <div className={style['photo-title']}>
                Zdjęcia użytkownika <span className={style['user-name']}>{friend.name}</span>
              </div>
              { friend.last_photos.map(photo => (
                <div className={style.single}>
                  <div className={grid.row}>
                    <div className={grid['col-md-4']}>
                      <img src={photo.uploaded_photo} className={style['single-photo']} alt={'img'} />
                    </div>
                    <div className={grid['col-md-8']}>
                      <div className={style['emotions-wrapper']}>
                        Na tym zdjęciu {friend.name} jest:
                        <br /><br />
                        {
                          (JSON.parse(photo.emotions)).map(emotion => (
                            <div className={style['single-emotion']}>
                              <span className={style['emo-color']}>{emotion.Type}</span> w {emotion.Confidence.toFixed(1)}%
                            </div>
                          )
                        )}
                      </div>
                    </div>
                  </div>
                </div>
                )
              )}
            </div>
          </div>
          }
        </div>
      </div>
    );
  }
}

export default userPanelFriendOne;
