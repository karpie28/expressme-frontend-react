/**
 *
 * Register Page
 *
 */
import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import style from './style.less';

function validate(name, surname, email, password, rpassword) {
  const errors = [];
  if (email.length === 0 || password.length === 0) {
    errors.push('Uzupełnij pola!');
  } else if ((email.split('').filter((x) => x === '@') || (email.indexOf('.') === -1)).length !== 1) {
    errors.push('Niepoprawny format email!');
  } else if (password !== rpassword) {
    errors.push('Podane hasła nie są identyczne!');
  } else if (password < 8) {
    errors.push('Podane hasło jest za krótkie, minimum 8 znaków!');
  } else if (name < 4 || surname < 4) {
    errors.push('Nazwa jest zbyt krótka!');
  }
  return errors;
}

class Register extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.state = {
      name: '',
      surname: '',
      email: '',
      password: '',
      rpassword: '',
      errors: [],
      redirect: false,
      registerd: false,
      birth_date: '1990-01-01 00:00:00',
    };
    this.onSubmit = this.onSubmit.bind(this);
  }


  onSubmit = (e) => {
    e.preventDefault();
    const { name, surname, email, password, rpassword, birth_date } = this.state;
    const errors = validate(name, surname, email, password, rpassword);
    if (errors.length > 0) {
      this.setState({ errors });
    } else {
      axios.post('https://api.expressme.pl/api/auth/register', { name, surname, email, password, rpassword, birth_date })
        .then((result) => {
          console.log(result);
          this.setRegister();
        })
        .catch((error) => {
          console.log(error);
          errors.push('Podano błędne dane!');
        });
    }
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  }

  setRegister = () => {
    this.setState({
      registerd: true,
    });
  }

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/login" />;
    }
  }

  renderRegiModal = () => {
    if (this.state.registerd) {
      return (
        <div className={style['register-modal']}>
          Zostałeś pomyślnie zarejestrowany, aktywuj swoje konto klikająć w link na mailu!
        </div>
      );
    }
  }

  render() {
    const { errors } = this.state;
    return (
      <div className={style.register}>
        { this.renderRedirect() }
        { this.renderRegiModal() }
        <div className={style['form-wrapper']}>
          <h1 className={style.title}>ExpressMe</h1>
          <p className={style.alt}>Register to enjoy emotions with us</p>
          <form method="post" onSubmit={this.onSubmit} className={style['form-box']}>
            { errors.map((error) => (
              <div key={error} className={style['error-box']}>
                <p>Błąd: {error}</p>
              </div>
            ))}
            <input
              value={this.state.name}
              onChange={(evt) => this.setState({ name: evt.target.value })}
              className={style.input}
              type="text"
              name="name"
              placeholder="Imię"
            />
            <input
              value={this.state.surname}
              onChange={(evt) => this.setState({ surname: evt.target.value })}
              className={style.input}
              type="text"
              name="surname"
              placeholder="Nazwisko"
            />
            <input
              value={this.state.email}
              onChange={(evt) => this.setState({ email: evt.target.value })}
              className={style.input}
              type="text"
              name="email"
              placeholder="E-mail"
            />
            <input
              value={this.state.password}
              onChange={(evt) => this.setState({ password: evt.target.value })}
              className={style.input}
              type="password"
              name="password"
              placeholder="Hasło"
            />
            <input
              value={this.state.rpassword}
              onChange={(evt) => this.setState({ rpassword: evt.target.value })}
              className={style.input}
              type="password"
              name="rpassword"
              placeholder="Powtórz hasło"
            />
            <button type="submit" className={style['submit-button']}>Register</button>
          </form>
          <div className={style.links}>
            <div className={style.account}>
              You already have account? <Link to="/login" className={style['account-link']}>Login</Link> instead!
            </div>
            <Link to="/" className={style['go-back']}>Go back to home page</Link>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
